package com.hermit.system.controller.admin;

import com.github.pagehelper.Page;
import com.hermit.server.domain.Role;
import com.hermit.server.dto.RoleDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.ResponseDto;
import com.hermit.server.dto.RoleUserDto;
import com.hermit.server.exception.ValidatorException;
import com.hermit.server.service.RoleService;
import com.hermit.server.service.RoleUserService;
import com.hermit.server.util.ValidatorUtil;
import org.apache.ibatis.annotations.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/role")
public class RoleController {

    private static final Logger LOG = LoggerFactory.getLogger(RoleController.class);
    public static final String BUSINESS_NAME = "角色";
    @Resource
    private RoleService roleService;


    @PostMapping("/list")
   public ResponseDto list(@RequestBody PageDto pageDto){
        LOG.info("pageDto: {}",pageDto);
        ResponseDto responseDto = new ResponseDto();
        roleService.list(pageDto);
        responseDto.setContent(pageDto);
        return responseDto;
    }

    @PostMapping("/save")
   public ResponseDto save(@RequestBody RoleDto roleDto){
        LOG.info("roleDto: {}",roleDto);
        /**保存校验*/
        ValidatorUtil.required(roleDto.getName(),"角色");
        ValidatorUtil.length(roleDto.getName(),"角色",1,50);
        ValidatorUtil.required(roleDto.getDesc(),"描述");
        ValidatorUtil.length(roleDto.getDesc(),"描述",1,100);

        ResponseDto responseDto = new ResponseDto();
        roleService.save(roleDto);
        responseDto.setContent(roleDto);
        return responseDto;
    }

    @DeleteMapping("/delete/{id}")
   public ResponseDto delete(@PathVariable String id){
        LOG.info("id: {}",id);
        ResponseDto responseDto = new ResponseDto();
        roleService.delete(id);
        return responseDto;
    }

    @PostMapping("/save-resource")
    public ResponseDto saveResource(@RequestBody RoleDto roleDto){
        LOG.info("保存角色资源关联开始");
        ResponseDto<RoleDto> responseDto = new ResponseDto<>();
        roleService.saveResource(roleDto);
        responseDto.setContent(roleDto);
        return responseDto;
    }

    @PostMapping("/list-resource/{roleId}")
    public ResponseDto listResource(@PathVariable String roleId){
        LOG.info("加载角色资源开始");
        ResponseDto responseDto = new ResponseDto();
        List<String> resourceIdList = roleService.listResource(roleId);
        responseDto.setContent(resourceIdList);
        return responseDto;
    }

    @PostMapping("/save-user")
    public ResponseDto saveUser(@RequestBody RoleDto roleDto){
        LOG.info("保存角色用户关联开始");
        ResponseDto<RoleDto> responseDto = new ResponseDto<>();
        roleService.saveUser(roleDto);
        responseDto.setContent(roleDto);
        return responseDto;
    }

    @PostMapping("/list-user/{roleId}")
    public ResponseDto listUser(@PathVariable String roleId){
        LOG.info("加载角色用户开始");
        ResponseDto responseDto = new ResponseDto();
        List<String> userIdList = roleService.listUser(roleId);
        responseDto.setContent(userIdList);
        return responseDto;
    }

}