package com.hermit.system.controller.admin;

import com.github.pagehelper.Page;
import com.hermit.server.domain.RoleUser;
import com.hermit.server.dto.RoleUserDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.ResponseDto;
import com.hermit.server.exception.ValidatorException;
import com.hermit.server.service.RoleUserService;
import com.hermit.server.util.ValidatorUtil;
import org.apache.ibatis.annotations.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/roleUser")
public class RoleUserController {

    private static final Logger LOG = LoggerFactory.getLogger(RoleUserController.class);
    public static final String BUSINESS_NAME = "角色用户关联";
    @Resource
    private RoleUserService roleUserService;

    @PostMapping("/list")
   public ResponseDto list(@RequestBody PageDto pageDto){
        LOG.info("pageDto: {}",pageDto);
        ResponseDto responseDto = new ResponseDto();
        roleUserService.list(pageDto);
        responseDto.setContent(pageDto);
        return responseDto;
    }

    @PostMapping("/save")
   public ResponseDto save(@RequestBody RoleUserDto roleUserDto){
        LOG.info("roleUserDto: {}",roleUserDto);
        /**保存校验*/
        ValidatorUtil.required(roleUserDto.getRoleId(),"角色");
        ValidatorUtil.required(roleUserDto.getUserId(),"用户");

        ResponseDto responseDto = new ResponseDto();
        roleUserService.save(roleUserDto);
        responseDto.setContent(roleUserDto);
        return responseDto;
    }

    @DeleteMapping("/delete/{id}")
   public ResponseDto delete(@PathVariable String id){
        LOG.info("id: {}",id);
        ResponseDto responseDto = new ResponseDto();
        roleUserService.delete(id);
        return responseDto;
    }

}