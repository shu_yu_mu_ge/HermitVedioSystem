/**定义全局缓存常量*/
SESSION_KEY_COURSE = "SESSION_KEY_COURSE";

SESSION_KEY_CHAPTER = "SESSION_KEY_CHAPTER";

SESSION_KEY_LOGIN_USER = "SESSION_KEY_LOGIN_USER";

//session是会话缓存，浏览器关闭后消失
SessionStorage = {
    get: function (key) {
        let v = sessionStorage.getItem(key);
        if (v && typeof (v) !== "undefined" && v !== "undefined") {
            return JSON.parse(v);
        }
    },
    set: function (key, data) {
        sessionStorage.setItem(key,JSON.stringify(data));
    },
    remove:function (key){
        sessionStorage.removeItem(key);
    },
    clearAll:function (){
        sessionStorage.clear();
    }
};