const Toast = Swal.mixin({
    toast:true,
    position:'top-end',
    showConfirmButton:false,
    timer:3000,
    timeProgressBar:true,
    onOpen:(toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave',Swal.resumeTimer)
    }
});

toast = {
    success:function (msg){
        Swal.fire({
            position:'top-end',
            type: "success",
            title:msg,
            showConfirmButton:false,
            timer: 3000
        });
    },

    error:function (msg){
        Swal.fire({
            position:'top-end',
            type: 'error',
            title:msg,
            showConfirmButton:false,
            timer: 3000
        });
    },

    warning:function (msg){
        Swal.fire({
            position:'top-end',
            type: 'warning',
            title:msg,
            showConfirmButton:false,
            timer: 3000
        });
    },
};