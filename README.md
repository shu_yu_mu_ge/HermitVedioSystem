# VW_yf_lzy

#### 介绍
前后端分离视频网站
Front end and back end separation video website


#### 软件开发技术栈
- 微服务框架/单应用框架  `Spring Cloud`
- 持久层框架 `Mybatis` + `Generator`
- 数据库 `MySQL`+`Navicat for MySQL`
- 前端框架 `Vue`
- 前端响应式框架 `BootStrap`
- 模板工具 `Freemarker`
- 代码管理工具 `Git`
- 开发工具 `IEDA`
- 分布式缓存 `Redis`
- 阿里云服务 `OSS`
- 项目构建管理工具 `Maven`


#### 项目说明

1. business模块：控制层代码
2. doc模块：sql语句编写模块，用mybatis-generator生成持久层代码
3. eureka模块：注册中心模块 
4. file模块：文件上传模块，上传地址为本地 E：HermitNoOne/file 
5. gateway模块：路由模块  登陆拦截
6. generator模块：集成freemarker，自动生成Dto，Controller，service，前端vue和枚举类代码
7. server模块：公共服务类模块，存放对数据库处理的所有代码，主要应用于前后端
8. system模块：控台私有服务模块，主要应用控台权限控制、登陆验证
9. admin模块： 控台前端界面 bootstrap3 ace模板
10. web模块：网站前端界面   bootstrap4 


#### 使用说明

1.  修改数据库信息 数据库版本，数据库账号密码
2.  保证存在 E：HermitNoOne/file 
3.  引入node_modules

#### 项目截图
![控台登陆页面](https://images.gitee.com/uploads/images/2020/0922/202851_3cfb2a74_7908235.png "登陆界面.PNG")
![控台分类页面](https://images.gitee.com/uploads/images/2020/0922/202911_18bc005e_7908235.png "分类管理界面.png")
![控台讲师管理页面](https://images.gitee.com/uploads/images/2020/0922/202933_8889401e_7908235.png "讲师管理界面.png")
![控台角色管理页面](https://images.gitee.com/uploads/images/2020/0922/202949_22cde34d_7908235.png "角色管理界面.png")
![控台课程管理页面](https://images.gitee.com/uploads/images/2020/0922/203013_703e1b23_7908235.png "课程管理界面.png")
![控台文件管理页面](https://images.gitee.com/uploads/images/2020/0922/203036_03dcb5a4_7908235.png "文件管理界面.png")
![控台用户管理页面](https://images.gitee.com/uploads/images/2020/0922/203102_5fea56f6_7908235.png "用户管理界面.png")
![控台资源管理页面](https://images.gitee.com/uploads/images/2020/0922/203122_4db55c87_7908235.png "资源管理界面.png")
![web主页面](https://images.gitee.com/uploads/images/2020/0922/203206_fff73a31_7908235.png "web主页面.png")
![web所有分类页面](https://images.gitee.com/uploads/images/2020/0922/203145_5594974b_7908235.png "web课程分类页面.png")


#### 开发人员

1. 杨凡
2. 李子杨



