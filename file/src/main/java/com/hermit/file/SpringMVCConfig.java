package com.hermit.file;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 暴露文件路径
 */

@Configuration
public class SpringMVCConfig implements WebMvcConfigurer {

    @Value("${file.path}")
    private String FILE_PATH;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/f/**").addResourceLocations("file:" + FILE_PATH);
    }

    //访问图片路径：http://127.0.0.1:9003/file/f/teacher/V34fzvND-logome.png
    //访问图片路径：http://127.0.0.1:9000/file/f/teacher/V34fzvND-logome.png
}
