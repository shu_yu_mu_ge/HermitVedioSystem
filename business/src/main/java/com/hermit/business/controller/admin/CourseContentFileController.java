package com.hermit.business.controller.admin;

import com.github.pagehelper.Page;
import com.hermit.server.domain.CourseContentFile;
import com.hermit.server.dto.CourseContentFileDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.ResponseDto;
import com.hermit.server.exception.ValidatorException;
import com.hermit.server.service.CourseContentFileService;
import com.hermit.server.util.ValidatorUtil;
import org.apache.ibatis.annotations.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/course-content-file")
public class CourseContentFileController {

    private static final Logger LOG = LoggerFactory.getLogger(CourseContentFileController.class);
    public static final String BUSINESS_NAME = "课程内容文件";
    @Resource
    private CourseContentFileService courseContentFileService;

    @GetMapping("/list/{courseId}")
   public ResponseDto list(@PathVariable String courseId){
        ResponseDto responseDto = new ResponseDto();
        List<CourseContentFileDto> fileDtoList = courseContentFileService.list(courseId);
        responseDto.setContent(fileDtoList);
        return responseDto;
    }

    @PostMapping("/save")
   public ResponseDto save(@RequestBody CourseContentFileDto courseContentFileDto){
        LOG.info("courseContentFileDto: {}",courseContentFileDto);
        /**保存校验*/
        ValidatorUtil.required(courseContentFileDto.getCourseId(),"课程ID");
        ValidatorUtil.length(courseContentFileDto.getName(),"文件名",1,100);
        ValidatorUtil.length(courseContentFileDto.getUrl(),"地址",1,100);

        ResponseDto responseDto = new ResponseDto();
        courseContentFileService.save(courseContentFileDto);
        responseDto.setContent(courseContentFileDto);
        return responseDto;
    }

    @DeleteMapping("/delete/{id}")
   public ResponseDto delete(@PathVariable String id){
        LOG.info("id: {}",id);
        ResponseDto responseDto = new ResponseDto();
        courseContentFileService.delete(id);
        return responseDto;
    }

}