package com.hermit.business.controller.admin;

import com.hermit.server.dto.CategoryDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.ResponseDto;
import com.hermit.server.service.CategoryService;
import com.hermit.server.util.ValidatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/category")
public class CategoryController {

    private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);
    public static final String BUSINESS_NAME = "分类";
    @Resource
    private CategoryService categoryService;

    @PostMapping("/list")
   public ResponseDto list(@RequestBody PageDto pageDto){
        LOG.info("pageDto: {}",pageDto);
        ResponseDto responseDto = new ResponseDto();
        categoryService.list(pageDto);
        responseDto.setContent(pageDto);
        return responseDto;
    }

    @PostMapping("/all")
    public ResponseDto all(){
        ResponseDto responseDto = new ResponseDto();
        List<CategoryDto> categoryDtoList = categoryService.all();
        responseDto.setContent(categoryDtoList);
        return responseDto;
    }

    @PostMapping("/save")
   public ResponseDto save(@RequestBody CategoryDto categoryDto){
        LOG.info("categoryDto: {}",categoryDto);
        /**保存校验*/
        ValidatorUtil.required(categoryDto.getParent(),"父ID");
        ValidatorUtil.required(categoryDto.getName(),"名称");
        ValidatorUtil.length(categoryDto.getName(),"名称",1,50);

        ResponseDto responseDto = new ResponseDto();
        categoryService.save(categoryDto);
        responseDto.setContent(categoryDto);
        return responseDto;
    }

    @DeleteMapping("/delete/{id}")
   public ResponseDto delete(@PathVariable String id){
        LOG.info("id: {}",id);
        ResponseDto responseDto = new ResponseDto();
        categoryService.delete(id);
        return responseDto;
    }

}