package com.hermit.server.exception;

public enum  BusinessExceptionCode {
    USER_LOGIN_NAME_EXIST("登录名已经存在"),
    USER_LOGIN_USERNAME_ERROR("用户名不存在"),
    USER_LOGIN_PASSWORD_ERROR("密码不正确"),
    HERMIT_SO_COOL("杨凡真TM帅");


    private String desc;

    BusinessExceptionCode(String desc){this.desc = desc;}

    public String getDesc(){return desc;}
    public void setDesc(String desc){this.desc = desc;}
}
