package com.hermit.server.exception;
/**
 * 自定义异常
 * 登陆用户名已存在异常
 */
public class BusinessException extends RuntimeException {
    private BusinessExceptionCode code;

    public BusinessException(BusinessExceptionCode code){
        super(code.getDesc());
        this.code = code;
    }

    public BusinessExceptionCode getCode(){return code;}

    public void setCode(BusinessExceptionCode code){
        this.code = code;
    }

    /**错误信息不写入堆栈信息，提高性能，重写*/

    @Override
    public Throwable fillInStackTrace(){ return this; }

}
