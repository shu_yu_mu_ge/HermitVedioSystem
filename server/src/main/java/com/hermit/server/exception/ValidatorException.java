package com.hermit.server.exception;

/**
 * 自定义异常
 * 校验异常
 */
public class ValidatorException extends RuntimeException{
    public ValidatorException(String message){ super(message); }
}
