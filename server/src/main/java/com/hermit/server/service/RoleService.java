package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.*;
import com.hermit.server.dto.RoleDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.mapper.RoleMapper;
import com.hermit.server.mapper.RoleResourceMapper;
import com.hermit.server.mapper.RoleUserMapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class RoleService {
    @Resource
    private RoleMapper roleMapper;

    @Resource
    private RoleResourceMapper roleResourceMapper;
    @Resource
    private RoleUserMapper roleUserMapper;

    /** 查询*/
    public void list(PageDto pageDto){
        PageHelper.startPage(pageDto.getPage(),pageDto.getSize());
        RoleExample roleExample = new RoleExample();
        /** 根据sort来排序*/

        List<Role> roleList = roleMapper.selectByExample(roleExample);
        //查询数据
        PageInfo<Role> pageInfo = new PageInfo<>(roleList);
        //设置已查询条数
        pageDto.setTotal(pageInfo.getTotal());

        List<RoleDto> roleDtoList = new ArrayList<RoleDto>();
        for (int i = 0; i < roleList.size(); i++) {
            Role role = roleList.get(i);
            RoleDto roleDto = new RoleDto();
            BeanUtils.copyProperties(role,roleDto);
            roleDtoList.add(roleDto);
        }
        //设置最终查询结果
        pageDto.setList(roleDtoList);
        //return pageDto;  pageDto是从前端传递来的，按道理应该有返回值，此处传入对象==返回对象，属于原地返回（杨凡专用名词）
    }
    /** 保存*/
    public void save(RoleDto roleDto){
        Role role = CopyUtil.copy(roleDto,Role.class);
        if(StringUtils.isEmpty(roleDto.getId())){
            this.insert(role);
        }else{
            this.update(role);
        }

    }
    /**新增 */
    private void insert(Role role){
        role.setId(UUidUtil.getShortUuid());
        roleMapper.insert(role);
    }
    /**修改更新 */
    private void update(Role role){
        roleMapper.updateByPrimaryKey(role);
    }
    /**删除 */
    public void delete(String id){
        roleMapper.deleteByPrimaryKey(id);
    }

    /**按照角色保存资源*/
    public void saveResource(RoleDto roleDto){
        String roleId = roleDto.getId();
        List<String> resourceId = roleDto.getResourceIds();
        //清空库中所有当前角色的记录
        RoleResourceExample example = new RoleResourceExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        roleResourceMapper.deleteByExample(example);

        //保存角色资源
        for(int i=0;i<resourceId.size();i++){
            RoleResource roleResource = new RoleResource();
            roleResource.setId(UUidUtil.getShortUuid());
            roleResource.setRoleId(roleId);
            roleResource.setResource(resourceId.get(i));
            roleResourceMapper.insert(roleResource);
        }
    }
    /**加载角色资源*/
    public List<String> listResource(String roleId){
        RoleResourceExample example = new RoleResourceExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        List<RoleResource> roleResourcesList = roleResourceMapper.selectByExample(example);
        List<String> resourceIdList = new ArrayList<>();
        for(int i=0,l=roleResourcesList.size();i<l;i++){
            resourceIdList.add(roleResourcesList.get(i).getResource());
        }
        return resourceIdList;
    }

    /**按照角色保存用户*/
    public void saveUser(RoleDto roleDto){
        String roleId = roleDto.getId();
        List<String> userId = roleDto.getUserIds();
        //清空库中所有当前角色的记录
        RoleUserExample example = new RoleUserExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        roleUserMapper.deleteByExample(example);

        //保存角色资源
        for(int i=0;i<userId.size();i++){
            RoleUser roleUser = new RoleUser();
            roleUser.setId(UUidUtil.getShortUuid());
            roleUser.setRoleId(roleId);
            roleUser.setUserId(userId.get(i));
            roleUserMapper.insert(roleUser);
        }
    }

    /**加载角色用户*/
    public List<String> listUser(String roleId){
        RoleUserExample example = new RoleUserExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        List<RoleUser> roleUserList = roleUserMapper.selectByExample(example);
        List<String> userIdList = new ArrayList<>();
        for(int i=0,l=roleUserList.size();i<l;i++){
            userIdList.add(roleUserList.get(i).getUserId());
        }
        return userIdList;

    }
}