package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.RoleResource;
import com.hermit.server.domain.RoleResourceExample;
import com.hermit.server.dto.RoleResourceDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.mapper.RoleResourceMapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class RoleResourceService {
    @Resource
    private RoleResourceMapper roleResourceMapper;

    /** 查询*/
    public void list(PageDto pageDto){
        PageHelper.startPage(pageDto.getPage(),pageDto.getSize());
        RoleResourceExample roleResourceExample = new RoleResourceExample();
        /** 根据sort来排序*/

        List<RoleResource> roleResourceList = roleResourceMapper.selectByExample(roleResourceExample);
        //查询数据
        PageInfo<RoleResource> pageInfo = new PageInfo<>(roleResourceList);
        //设置已查询条数
        pageDto.setTotal(pageInfo.getTotal());

        List<RoleResourceDto> roleResourceDtoList = new ArrayList<RoleResourceDto>();
        for (int i = 0; i < roleResourceList.size(); i++) {
            RoleResource roleResource = roleResourceList.get(i);
            RoleResourceDto roleResourceDto = new RoleResourceDto();
            BeanUtils.copyProperties(roleResource,roleResourceDto);
            roleResourceDtoList.add(roleResourceDto);
        }
        //设置最终查询结果
        pageDto.setList(roleResourceDtoList);
        //return pageDto;  pageDto是从前端传递来的，按道理应该有返回值，此处传入对象==返回对象，属于原地返回（杨凡专用名词）
    }
    /** 保存*/
    public void save(RoleResourceDto roleResourceDto){
        RoleResource roleResource = CopyUtil.copy(roleResourceDto,RoleResource.class);
        if(StringUtils.isEmpty(roleResourceDto.getId())){
            this.insert(roleResource);
        }else{
            this.update(roleResource);
        }

    }
    /**新增 */
    private void insert(RoleResource roleResource){
        roleResource.setId(UUidUtil.getShortUuid());
        roleResourceMapper.insert(roleResource);
    }
    /**修改更新 */
    private void update(RoleResource roleResource){
        roleResourceMapper.updateByPrimaryKey(roleResource);
    }
    /**删除 */
    public void delete(String id){
        roleResourceMapper.deleteByPrimaryKey(id);
    }
}