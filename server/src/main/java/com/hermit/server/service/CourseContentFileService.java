package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.CourseContentFile;
import com.hermit.server.domain.CourseContentFileExample;
import com.hermit.server.dto.CourseContentFileDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.mapper.CourseContentFileMapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class CourseContentFileService {
    @Resource
    private CourseContentFileMapper courseContentFileMapper;

    /** 查询*/
    public List<CourseContentFileDto> list(String courseId){
        CourseContentFileExample example = new CourseContentFileExample();
        CourseContentFileExample.Criteria criteria = example.createCriteria();
        criteria.andCourseIdEqualTo(courseId);
        List<CourseContentFile> fileList = courseContentFileMapper.selectByExample(example);
        return CopyUtil.copyList(fileList,CourseContentFileDto.class);
    }
    /** 保存*/
    public void save(CourseContentFileDto courseContentFileDto){
        CourseContentFile courseContentFile = CopyUtil.copy(courseContentFileDto,CourseContentFile.class);
        if(StringUtils.isEmpty(courseContentFileDto.getId())){
            this.insert(courseContentFile);
        }else{
            this.update(courseContentFile);
        }

    }
    /**新增 */
    private void insert(CourseContentFile courseContentFile){
        courseContentFile.setId(UUidUtil.getShortUuid());
        courseContentFileMapper.insert(courseContentFile);
    }
    /**修改更新 */
    private void update(CourseContentFile courseContentFile){
        courseContentFileMapper.updateByPrimaryKey(courseContentFile);
    }
    /**删除 */
    public void delete(String id){
        courseContentFileMapper.deleteByPrimaryKey(id);
    }
}