package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.UserInfo;
import com.hermit.server.domain.UserInfoExample;
import com.hermit.server.dto.LoginUserDto;
import com.hermit.server.dto.UserInfoDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.exception.BusinessException;
import com.hermit.server.exception.BusinessExceptionCode;
import com.hermit.server.mapper.UserInfoMapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class UserInfoService {

    private static final Logger LOG = LoggerFactory.getLogger(UserInfoService.class);
    @Resource
    private UserInfoMapper userInfoMapper;

    /** 查询*/
    public void list(PageDto pageDto){
        PageHelper.startPage(pageDto.getPage(),pageDto.getSize());
        UserInfoExample userInfoExample = new UserInfoExample();
        /** 根据sort来排序*/

        List<UserInfo> userInfoList = userInfoMapper.selectByExample(userInfoExample);
        //查询数据
        PageInfo<UserInfo> pageInfo = new PageInfo<>(userInfoList);
        //设置已查询条数
        pageDto.setTotal(pageInfo.getTotal());

        List<UserInfoDto> userInfoDtoList = new ArrayList<UserInfoDto>();
        for (int i = 0; i < userInfoList.size(); i++) {
            UserInfo userInfo = userInfoList.get(i);
            UserInfoDto userInfoDto = new UserInfoDto();
            BeanUtils.copyProperties(userInfo,userInfoDto);
            userInfoDtoList.add(userInfoDto);
        }
        //设置最终查询结果
        pageDto.setList(userInfoDtoList);
        //return pageDto;  pageDto是从前端传递来的，按道理应该有返回值，此处传入对象==返回对象，属于原地返回（杨凡专用名词）
    }
    /** 保存*/
    public void save(UserInfoDto userInfoDto){
        UserInfo userInfo = CopyUtil.copy(userInfoDto,UserInfo.class);
        if(StringUtils.isEmpty(userInfoDto.getId())){
            this.insert(userInfo);
        }else{
            this.update(userInfo);
        }

    }
    /**新增 */
    private void insert(UserInfo userInfo){
        userInfo.setId(UUidUtil.getShortUuid());
        UserInfo info = this.selectByLoginName(userInfo.getUsername());
        if(info != null){
            throw new BusinessException(BusinessExceptionCode.USER_LOGIN_NAME_EXIST);
        }
        userInfoMapper.insert(userInfo);
    }
    /**修改更新 */
    private void update(UserInfo userInfo){
        userInfo.setPassword(null);
        userInfoMapper.updateByPrimaryKeySelective(userInfo);
    }
    /**删除 */
    public void delete(String id){
        userInfoMapper.deleteByPrimaryKey(id);
    }
    /**查询数据库登陆名记录*/
    public UserInfo selectByLoginName(String username){
        UserInfoExample example = new UserInfoExample();
        example.createCriteria().andUsernameEqualTo(username);
        List<UserInfo> userInfoList = userInfoMapper.selectByExample(example);
        if(CollectionUtils.isEmpty(userInfoList)){
            return null;
        }else{
            return userInfoList.get(0);
        }
    }
    /**修改密码*/
    public void savePassword(UserInfoDto userInfoDto){
        UserInfo userInfo = new UserInfo();
        userInfo.setId(userInfoDto.getId());
        userInfo.setPassword(userInfoDto.getPassword());
        userInfoMapper.updateByPrimaryKeySelective(userInfo);
    }

    /**用户登陆*/
    public LoginUserDto login(UserInfoDto userInfoDto){
        UserInfo userInfo = selectByLoginName(userInfoDto.getUsername());
        if(userInfo == null){
            //账号不存在
            LOG.info("账号不存在 用户名:{} ",userInfoDto.getUsername());
            throw new BusinessException(BusinessExceptionCode.USER_LOGIN_USERNAME_ERROR);
        }else{
            if(userInfo.getPassword().equals(userInfoDto.getPassword())){
                //登陆成功
                LOG.info("登陆成功");
                return CopyUtil.copy(userInfo,LoginUserDto.class);
            }else{
                //密码不对
                LOG.info("密码不正确 输入密码:{},数据库密码:{} ",userInfoDto.getPassword(),userInfo.getPassword());
                throw new BusinessException(BusinessExceptionCode.USER_LOGIN_PASSWORD_ERROR);
            }
        }
    }
}