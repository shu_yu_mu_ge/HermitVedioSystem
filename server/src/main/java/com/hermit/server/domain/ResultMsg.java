package com.hermit.server.domain;

/**
 * @Author: Answer
 * @Date: 2020/8/31 22:12
 * @Description: *
 */
public class ResultMsg {
    private boolean result;
    private String msg;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
