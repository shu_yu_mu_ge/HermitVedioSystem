package com.hermit.server.util;

import com.hermit.server.exception.ValidatorException;
import org.springframework.util.StringUtils;

/**
 * 后端校验
 * 防止绕过前端直接访问后端接口处理数据 例如：postman
 */
public class ValidatorUtil {
    /**
     * 空校验（null & “ ”）
     */
    public static void required(Object str,String fieldName){
        if(StringUtils.isEmpty(str)){
            throw new ValidatorException(fieldName+"不能为空(后端校验）");
        }
    }

    /**
     * 长度校验
     */
    public static void length(String str,String fieldName,int min,int max){
        if(StringUtils.isEmpty(str)){
            return;
        }
        int length = 0;
        if(!StringUtils.isEmpty(str)){
            length = str.length();
        }
        if(length<min||length>max){
            throw new ValidatorException(fieldName+"长度"+min+"-"+max+"位!（后端校验）");
        }
    }
}
