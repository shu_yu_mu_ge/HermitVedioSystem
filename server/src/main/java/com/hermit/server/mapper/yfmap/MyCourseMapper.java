package com.hermit.server.mapper.yfmap;

import com.hermit.server.dto.SortDto;
import org.apache.ibatis.annotations.Param;

public interface MyCourseMapper {

    int updateTime(@Param("courseId") String courseId);

    int updateSort(SortDto sortDto);

    int moveSortsBackword(SortDto sortDto);

    int moveSortsForward(SortDto sortDto);
}
