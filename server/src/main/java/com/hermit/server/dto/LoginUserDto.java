package com.hermit.server.dto;

/** Dto类 */
public class LoginUserDto {
    private String id;
    private String username;
    private String name;
    private String token;
    /**set与get方法  */
    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(",id=").append(id);
        sb.append(",username=").append(username);
        sb.append(",name=").append(name);
        sb.append(",token=").append(token);
        sb.append("]");
        return sb.toString();
    }
}